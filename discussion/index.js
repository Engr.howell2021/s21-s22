//[SECTION] Data Modeling

//1. Identify what information we want to gather from the customers in order to determine wether the user's identity is true. 


	//=> WHY? 
   //1. TO PROPERLY PLAN out what information will be deemed useful.
   //2. to lessen the chances or scenarios of having to modify or edit the data stored in the database.
   //3. to anticipate how this data/information would relate to each other.

	//TASK: Create a Course Booking System for an Institution. 

     //What are the 'minimum information' would i need to collect from the customers/students. 
     //the following information described below would identify the structure of the user in our app
     
     //User Documents
	     // 1. first Name
	     // 2. last Name
	     // 3. Middle Name
	     // 4. email address
	     // 5. PIN/password
	     // 6. mobile number 
	     // 7. birthdate 
	     // 8. gender
	     // 9. isAdmin: role and restriction/limitations that this user would have in our app. 
	     //10. dateTimeRegistered => when the student signed up/enrolled in the institution. 

	//Course/Subjects 

	    //1. name/title
	    //2. course code 
	    //3. course description
	    //4. course units
	    //5. course instructor
	    //6. isActive => to describe if the course is being offered by the institution. 
	    //7. dateTimeCreated => for us to identify when the course was added to the database. 
	    //8. available slots


     //As a full stack web developer (front, backend, database)
     //The more information you gather the more difficult it is to scale and manage the collection. it is going more difficult to maintain. 



//2. Create an ERD to represent the entities inside the database as well as to describe the relationships amongst them. 


     //users can have multiple subjects
     //subjects can have multiple enrollees

     //user can have multiple transactions
     //transaction can only belong to a single user. 

     //a single transaction can have multiple courses
     //a course can be part of mutiple transactions
    
    //ill give you 5 minute to export your ERD_course_booking and save JPEG, PDF, PNG file inside the discussion folder. 


//3. Convert and Translate the ERD into a JSON-like syntax in order to describe the structure of the document inside the collection by creating a MOCK data. 

      //NOTE: Creating/using mock data can play an integral role during the testing phase of any app development.

      //username: martin@email.com-> this is one one of the things you need to avoid.

      //you want to be able to utilize the storage in your database. 

      //you can use this tool to generate mock data seamlessly:
        //GO TO THIS LINK: https://www.mockaroo.com/
      
